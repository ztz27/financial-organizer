import javax.swing.JOptionPane;


public class AccountManager {
	
	public static String select() {
//Creates 2 options in JOptionPane for the user to choose from	
		String[] choices1 = {"Checking Account", "Credit Card"};
		String input1 = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type", JOptionPane.QUESTION_MESSAGE , null, choices1, choices1 [0]);
		return input1;
	}
//Creates months paired with characters that are displayed after the user selects the account	
		public static String select2(String accountType) {
			String[] choices2 = {"A. January", "B. February", "C. March", "D. April", "E. May", "F. June", "G. July", "H. August", "I. September", "J. October", "K. November", "L. December"};
			String input2 = null;
//
				if (accountType == "Checking Account") {
				input2 = (String) JOptionPane.showInputDialog(null, "Select Month...", "Month", JOptionPane.QUESTION_MESSAGE , null, choices2, choices2 [0]);
			
			}
			
				else if (accountType == "Credit Card") {
				input2 = (String) JOptionPane.showInputDialog(null, "Select Month...", "Month", JOptionPane.QUESTION_MESSAGE , null, choices2, choices2 [0]);
			}
			
			return input2;
			
		}
	
	}


