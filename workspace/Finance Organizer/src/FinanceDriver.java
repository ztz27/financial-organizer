//This is the Main that calls the other class methods, I tried to keep it as clean and as simple as possible
public class FinanceDriver {
	public static void main(String[]args){
//Authenticator		
		if (Authenticator.authenticate()) {
//Account Manager			
			String accountType = AccountManager.select(); 
//Checking Account			
			if (accountType == "Checking Account") {
//Checking Account Overview organized by month		
				CheckingOverview.disp();
			}
//Credit Card		
			else if (accountType == "Credit Card") {
//Credit Card Statements organized by month				
				CreditCardOverview.disp();
			}
			
		
		} 
	}
}
