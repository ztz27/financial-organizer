//Import Java io to read PDF File
import java.io.*;
//Checking Account Overview
	public class CheckingOverview {
	public static void disp() {
		String fileAddress = null;
//Instantiate the character variable from Account Manager to be used with Checking Overview		
			char month = AccountManager.select2("Checking Account").charAt(0);
//January Statement		
			switch (month) {
				case 'A':
//*Using this code I found online I was able to import PDF files from my computer into the Java Program				
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/12. January 23rd - February 23rd ~ 2014.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//February Statement			
				case 'B':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/13. February 24th - March 20th ~ 2014.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//March Statement				
				case 'C':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/14. March 21st - April 20th ~ 2014.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//April Statement			
				case 'D':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/3. April 19th - May 20th ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//May Statement			
				case 'E':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/4. May 21st - June 20th ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//June Statement			
				case 'F':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/5. June 21st - July 21st ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//July Statement			
				case 'G':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/6. July 22nd - August 20th ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//August Statement				
				case 'H':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/7. August 21st - September 22nd ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//September Statement			
				case 'I':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/8. September 23rd - October 21st ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//October Statement			
				case 'J':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/9. October 22nd -  November 21st ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//November Statement			
				case 'K':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/10. November 22nd - December 19th ~ 2013.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
//December Statement			
				case 'L':
					
					fileAddress = "C:/Users/Zimmers/Desktop/Bank Statements - Burke & Herbert/11. December 20th - January 22nd ~ 2014.pdf";
					try {
						if ((new File(fileAddress)).exists()) {
							Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + fileAddress);
							
					} 
						else {
						System.out.println("File Not Found");
					}
						
				}catch (Exception e) {
					System.out.println(":: -----Exception---- ::\n"+e);
				}
			break;
			
			}

	}
		
	
}
	
	
