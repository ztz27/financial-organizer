//Import JOptionPane from javax library
import javax.swing.JOptionPane;																		

public class Authenticator {
public static boolean authenticate(){
//Set loop counter
	int counter = 0; 
	boolean authenticated = false;
//Set counter limit: 3 password attempts before lock
	while(counter<3) {																																					
//Prompt user for username input		
		String stringUserName = JOptionPane.showInputDialog("Please Enter Username");       
		if (stringUserName.equalsIgnoreCase("Zachary")){
//If username is correct, prompt user for password entry			
			String password = JOptionPane.showInputDialog("Please Enter Password");				
			if (password.equals("password")) {																
				JOptionPane.showMessageDialog(null, "Welcome Zachary");						
				authenticated = true;
//If credentials match, break loop
				break;
//If incorrect password is entered return message "incorrect password"
			} else {
				JOptionPane.showMessageDialog(null, "Incorrect Password");
				++counter;
				continue;
			}	
		} else {
			continue;
		}
				
	}
	
	if (authenticated) {
		System.out.println("Access Granted");
//If password is entered incorrectly 3 times then return "access denied message"	
	} else {
		JOptionPane.showMessageDialog(null, "Access Denied");				
		System.out.println("Access Denied");	
	}

	return authenticated;
	}
}